elasticdump --input=https://er.uniqa-international.eu/es --input-index=prod_data --output=http://localhost:9200 --output-index=dev_data --type=data --limit=10 --searchBody='{"size":2000,"from":10000,"query":{"nested":{"path":"lob.addresses","query":{"exists":{"field":"lob.addresses.id"}}}}}'

elasticdump --input=https://er.uniqa-international.eu/es --input-index=prod_addresses --output=http://localhost:9200 --output-index=dev_addresses --type=data --limit=10 --concurrency=12 --overwrite=true --searchBody='{"size":1000,"from":10000,"query":{"nested":{"path":"address","query":{"exists":{"field":"address.id"}}}}}'

# Decommissioned Bonsai
elasticdump --input=https://b1q0rm2yiy:9xzobqgyrl@joes-1st-cluster-2474232894.eu-central-1.bonsaisearch.net:443/dev_addresses --output=http://localhost:9200/dev_addresses --type=data

# GCP BITNAMI ELK
# data mapping
elasticdump --input=http://localhost:9200 --input-index=dev_data --output=http://user:PRwWYskSMb4y@34.107.27.170/elasticsearch --output-index=dev_data --type=mapping
# data data
elasticdump --input=http://localhost:9200 --input-index=dev_data --output=http://user:PRwWYskSMb4y@34.107.27.170/elasticsearch --output-index=dev_data --type=data

# address mapping
elasticdump --input=http://localhost:9200 --input-index=dev_addresses --output=http://user:PRwWYskSMb4y@34.107.27.170/elasticsearch --output-index=dev_addresses --type=mapping
# address data
elasticdump --input=http://localhost:9200 --input-index=dev_addresses --output=http://user:PRwWYskSMb4y@34.107.27.170/elasticsearch --output-index=dev_addresses --type=data
