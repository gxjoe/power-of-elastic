import json
import re


def read_file(path, as_json=True):
    with open(path) as f:
        read_ = f.read()

        if as_json:
            try:
                return json.loads(read_)
            except Exception as e:
                print(e)
                return None

        return read_


def compose(*functions):
    def inner(arg):
        for f in reversed(functions):
            arg = f(arg)
        return arg
    return inner


def write_file(path, data, as_json=True):
    with open(path, 'wt+') as f:
        f.write(json.dumps(data, indent=4) if as_json else data)


def extract_and_convert(regex, incoming, callback):
    return re.sub(regex,
                  callback,
                  incoming)
